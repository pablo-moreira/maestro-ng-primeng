import { Component, Input } from '@angular/core';
import { OverlayPanel } from 'primeng/components/overlaypanel/overlaypanel';
import { Operator } from '../../models/operator.interface';
import { Restriction } from '@maestro-ng/core';
import { RestrictionOperator } from '@maestro-ng/core';

@Component({
  selector: 'm-entity-restriction',
  templateUrl: './entity-restriction.component.html',
  styleUrls: ['./entity-restriction.component.css'],
})
export class EntityRestrictionComponent {

  @Input() public label: string;
  @Input() public items: any[];
  @Input() public itemId: string;
  @Input() public itemLabel: string;
  @Input() public restriction: Restriction<any>;

  public operators: Operator[] = [];

  constructor() {
    this.operators.push({ id: RestrictionOperator.EQUALS, label: 'Igual' });
    this.operators.push({ id: RestrictionOperator.NOT_EQUALS, label: 'Diferente' });
    this.operators.push({ id: RestrictionOperator.IN, label: 'Dentro das opções' });
    this.operators.push({ id: RestrictionOperator.NOT_IN, label: 'Fora das opções' });
    this.operators.push({ id: RestrictionOperator.IS_NOT_NULL, label: 'Com definição' });
    this.operators.push({ id: RestrictionOperator.IS_NULL, label: 'Sem definição' });
  }

  public getName(): string {
    return `restriction_${this.restriction.attribute.replace('.', '_')}_value`;
  }
}
