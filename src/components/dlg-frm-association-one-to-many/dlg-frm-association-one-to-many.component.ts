import { Component, Input } from '@angular/core';
import { DlgFrmAssociationOneToMany } from '@maestro-ng/core';


@Component({
  selector: 'm-dlg-frm-association-one-to-many',
  templateUrl: './dlg-frm-association-one-to-many.component.html',
  styleUrls: ['./dlg-frm-association-one-to-many.component.css'],
})
export class DlgFrmAssociationOneToManyComponent {

  @Input() public header: string;
  @Input() public modal = true;
  @Input() public appendTo = 'body';
  @Input() public contentStyle: any = { overflow: 'visible' };
  @Input() public association: DlgFrmAssociationOneToMany<any, any, any, any>;

  constructor() {}
}