import { Component, Input } from '@angular/core';
import { OverlayPanel } from 'primeng/components/overlaypanel/overlaypanel';
import { Operator } from '../../models/operator.interface';
import { Restriction } from '@maestro-ng/core';

@Component({
  selector: 'm-label-restriction',
  templateUrl: './label-restriction.component.html',
  styleUrls: ['./label-restriction.component.css']
})
export class LabelRestrictionComponent {

  @Input() public label: string;
  @Input() public restriction: Restriction<any>;
  @Input() public operators: Operator[];

  constructor() {}

  public getOperatorLabel(): string {
    return this.operators.filter(op => this.restriction.operator === op.id)[0].label;
  }

  public onChangeOperator(op: OverlayPanel): void {
    this.restriction.onChangeOperator();
    op.hide();
  }
}
