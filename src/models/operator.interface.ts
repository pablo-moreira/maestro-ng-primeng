import { RestrictionOperator } from '@maestro-ng/core';

export interface Operator {
  id: RestrictionOperator;
  label: string;
}
