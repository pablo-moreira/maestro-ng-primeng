import { LazyLoadEvent } from 'primeng/components/common/lazyloadevent';
import { Restriction, IRestrictions, EntityService, IProgressService, IMessageService, SimplePagedQuery } from '@maestro-ng/core';
import { SortMeta } from 'primeng/components/common/sortmeta';

export class ServiceDataSource<E, I> {

  public entities: E[];
  public lastLoadEvent: LazyLoadEvent;
  public total: number;
  public restrictions: IRestrictions;

  constructor(
    private service: EntityService<E, I>,
    private messageService: IMessageService,
    private progressService: IProgressService,
    public rows = 10) {
  }

  public onLazyLoad(event: LazyLoadEvent): void {

    let restrictionsList: Array<Restriction<any>>;

    if (this.restrictions !== undefined) {
      restrictionsList = this.restrictions.getEnables();
    }

    const sq = this.newSimplePagedQuery(event, restrictionsList);
    this.progressService.showModeless();
    this.service.retrievePaged(sq)
      .then(result => {
        this.entities = result.entities;
        this.lastLoadEvent = event;
        this.total = result.count;
        this.progressService.hide();
      })
      .catch(result => {
        this.messageService.addError('Erro', 'Erro ao carregar os dados no servidor');
        this.progressService.hide();
      });
  }

  public load(): void {
    this.onLazyLoad({ first: 0, rows: this.rows });
  }

  public refresh(): void {
    this.onLazyLoad(this.lastLoadEvent);
  }

  public clearRestrictions(): void {
    this.restrictions.clear();
    this.onLazyLoad(this.lastLoadEvent);
  }

  public search(): void {

    const event = {
      first: 0,
      rows: this.rows,
      sortField: undefined as string,
      sortOrder: undefined as number,
      multiSortMeta: undefined as SortMeta[]
    };

    if (this.lastLoadEvent !== undefined) {
      event.sortField = this.lastLoadEvent.sortField;
      event.sortOrder = this.lastLoadEvent.sortOrder;
      event.multiSortMeta = this.lastLoadEvent.multiSortMeta;
    }

    this.onLazyLoad(event);
  }

  public onEnterSearch(event: any): void {
    if (event.keyCode === 13) {
      this.search();
    }
  }

  private newSimplePagedQuery(event: LazyLoadEvent, restrictions: Array<Restriction<any>>): SimplePagedQuery {

    const query: SimplePagedQuery = {
      firstResult: event.first,
      maxResult:  event.rows,
      sorts: [],
      restrictions: []
    };

    if (restrictions && restrictions.length > 0) {
      query.restrictions = restrictions;
    }

    if (event.multiSortMeta) {
      event.multiSortMeta.forEach(s => {
        query.sorts.push({
          attribute: s.field,
          order: s.order === -1 ? 'DESC' : 'ASC'
        });
      });
    }
    else if (event.sortField) {
      query.sorts.push({
        attribute: event.sortField,
        order: event.sortOrder === -1 ? 'DESC' : 'ASC'
      });
    }

    return query;
  }
}
