import { InputTextModule } from 'primeng/components/inputtext/inputtext';
import { ChipsModule } from 'primeng/components/chips/chips';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { ConfirmationService as CsPrimeng } from 'primeng/components/common/confirmationservice';
import { MessageService as MsPrimeng } from 'primeng/components/common/messageservice';
import { MessagesModule } from 'primeng/components/messages/messages';
import { MessageModule } from 'primeng/components/message/message';
import { NgModule } from '@angular/core';
import { GrowlModule } from 'primeng/components/growl/growl';
import { ProgressBarModule } from 'primeng/components/progressbar/progressbar';
import { CommonModule } from '@angular/common';
import { RadioButtonModule } from 'primeng/components/radiobutton/radiobutton';
import { CheckboxModule } from 'primeng/components/checkbox/checkbox';
import { DropdownModule } from 'primeng/components/dropdown/dropdown';
import { AutoCompleteModule } from 'primeng/components/autocomplete/autocomplete';
import { OverlayPanelModule } from 'primeng/components/overlaypanel/overlaypanel';
import { DlgFrmAssociationOneToManyComponent } from './components/dlg-frm-association-one-to-many/dlg-frm-association-one-to-many.component';
import { EntityRestrictionComponent } from './components/entity-restriction/entity-restriction.component';
import { LabelRestrictionComponent } from './components/label-restriction/label-restriction.component';
import { StringRestrictionComponent } from './components/string-restriction/string-restriction.component';
import { NumberRestrictionComponent } from './components/number-restriction/number-restriction.component';
import { ProgressService } from './services/progress-service.service';
import { MessageService } from './services/message-service.service';
import { ConfirmationService } from './services/confirmation-service.service';
import { DialogModule } from 'primeng/components/dialog/dialog';
import { ButtonModule } from 'primeng/components/button/button';

@NgModule({
  imports: [
    // Ng
    BrowserModule,
    CommonModule,
    FormsModule,

    // Primeng
    AutoCompleteModule,
    ChipsModule,
    GrowlModule,
    MessageModule,
    MessagesModule,
    OverlayPanelModule,
    ProgressBarModule,
    CheckboxModule,
    DropdownModule,
    InputTextModule,
    RadioButtonModule,
    DialogModule,
    ButtonModule
  ],
  declarations: [
    // Maestro
    LabelRestrictionComponent,
    EntityRestrictionComponent,
    StringRestrictionComponent,
    NumberRestrictionComponent,
    DlgFrmAssociationOneToManyComponent
  ],
  exports: [
    // Maestro
    LabelRestrictionComponent,
    EntityRestrictionComponent,
    StringRestrictionComponent,
    NumberRestrictionComponent,
    DlgFrmAssociationOneToManyComponent,

    // Primeng
    AutoCompleteModule,
    ChipsModule,
    GrowlModule,
    MessageModule,
    MessagesModule,
    OverlayPanelModule,
    ProgressBarModule,
    CheckboxModule,
    DropdownModule,
    InputTextModule,
    RadioButtonModule,
    DialogModule,
    ButtonModule
  ],
  providers: [
    ConfirmationService,
    MessageService,
    ProgressService,
    CsPrimeng,
    MsPrimeng
  ]
})
export class MaestroPrimengModule { }
